﻿using UnityEngine;
using System.Diagnostics;

public class CreateMegaCube : MonoBehaviour
{

    public GameObject SpawnObject;
    public Stopwatch timer;
    public int NumberOfCubes;

    void Start()
    {
        timer = new Stopwatch();
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            timer.Start();
            SpawnCube();
            timer.Stop();
            UnityEngine.Debug.LogError((NumberOfCubes * NumberOfCubes * NumberOfCubes) + " kuubi loomiseks kulus:" + timer.Elapsed);
        }
    }
    private void SpawnCube()
    {
        for (int i = 0; i < NumberOfCubes; i++)
        {
            for (int j = 0; j < NumberOfCubes; j++)
            {
                for (int k = 0; k < NumberOfCubes; k++)
                {
                    Instantiate(SpawnObject, new Vector3(i, j, k), new Quaternion(0, 0, 0, 0));
                }
            }
        }
    }
}
