﻿using UnityEngine;
using System.Collections;

public class SpawnScript : MonoBehaviour
{
    public GameObject SpawnObject;

    private Random _random;
    private long _count;
    public float updateInterval = 0.5F;

    private float accum = 0; // FPS accumulated over the interval
    private int frames = 0; // Frames drawn over the interval
    private float timeleft; // Left time for current interval
    private float currentFrames;
    // Use this for initialization
    void Start()
    {
        timeleft = updateInterval;
    }

    // Update is called once per frame
    void Update()
    {
        int x = Random.Range(0, 100);
        int y = Random.Range(0, 100);
        int z = Random.Range(0, 100);
        Instantiate(SpawnObject, new Vector3(x, y, z), new Quaternion(0, 0, 0, 0));

        _count++;


        timeleft -= Time.deltaTime;
        accum += Time.timeScale / Time.deltaTime;
        ++frames;

        // Interval ended - update GUI text and start new interval
        if (timeleft <= 0.0)
        {
            currentFrames = accum / frames;
            timeleft = updateInterval;
            accum = 0.0F;
            frames = 0;
        }
        if (currentFrames < 10)
        {
            Debug.Log(_count);
            if (_count > 30)
            {
                Debug.Break();
            }
        }
    }
}
